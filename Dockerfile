ARG FUNCTION_DIR="/function"

FROM python:3.9-alpine AS aws-lambdaric-base-image

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.8

ARG FUNCTION_DIR

RUN apk add --no-cache --virtual .build-deps \
    gcc \
    g++ \
    make \
    cmake \
    unzip \
    libffi-dev \
    autoconf \
    automake \
    libtool \
    libexecinfo-dev && \
    python3 -m pip install --target ${FUNCTION_DIR} awslambdaric && \
    apk --purge del .build-deps && \
    apk add --no-cache libstdc++ curl-dev

WORKDIR ${FUNCTION_DIR}

ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/bin/aws-lambda-rie


COPY ./entrypoint.sh /
RUN chmod 755 /usr/bin/aws-lambda-rie /entrypoint.sh

ENTRYPOINT [ "sh", "/entrypoint.sh" ]
CMD ["app.lambda.handler"]


FROM python:3.9-alpine AS project-base-image

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.8
    
ARG FUNCTION_DIR
ARG SETUP_ENV

COPY ./pyproject.toml ./pyproject.toml
COPY ./poetry.lock ./poetry.lock

RUN apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev build-base && \
    python3 -m pip install poetry
RUN if [ ${SETUP_ENV} = "local" ] ; then \
    python3 -m poetry export -f requirements.txt --dev --without-hashes --output requirements.txt; \
else \
    python3 -m poetry export -f requirements.txt  --without-hashes --output requirements.txt; \
fi
RUN python3 -m pip install --target ${FUNCTION_DIR} -r requirements.txt && \
    python3 -m pip uninstall -y poetry
    
COPY ./app ${FUNCTION_DIR}/app
COPY ./README.md  ${FUNCTION_DIR}/README.md

FROM aws-lambdaric-base-image as dev-image

ARG FUNCTION_DIR

COPY --from=project-base-image ${FUNCTION_DIR} ${FUNCTION_DIR}
