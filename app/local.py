import uvicorn

from app.app import app

if __name__ == "__main__":
    try:
        import uvloop
    except ModuleNotFoundError:
        print("[*] Running without `uvloop`")
        uvloop = ...

    if uvloop is not ...:
        uvloop.install()

    uvicorn.run(app, host="0.0.0.0", port=8080, log_level="debug")
