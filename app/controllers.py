import io
import json

import aioboto3
from starlette import status
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse

from app.config import config
from app.schemas.responses import LambdaResponse


async def upload_file_controller(request: Request):
    try:
        form = await request.form()
        file_io = io.BytesIO(await form["file"].read())
        filename = form["file"].filename
    except json.JSONDecodeError:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Serwis wspiera jedynie REST'owe żądanie w formacie JSON.",
            headers={"WWW-Authenticate": "Bearer"},
        )
    session = aioboto3.Session()
    async with session.client("s3") as s3:
        await s3.upload_fileobj(file_io, config.AWS_S3_BUCKET, f"{filename}")
    return JSONResponse(LambdaResponse(url=f"https://{config.AWS_S3_BUCKET}.s3.{config.AWS_S3_REGION}.amazonaws.com/{filename}").dict(), status.HTTP_200_OK)
