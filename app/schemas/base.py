from pydantic import BaseModel


def to_snake_case(string: str) -> str:
    return "".join(["_" + i.lower() if i.isupper() else i for i in string]).lstrip("_")


def to_camel_case(string: str) -> str:
    return "".join(word.capitalize() for word in string.split("_"))


class Base(BaseModel):
    class Config:
        allow_population_by_field_name = True


class BaseSnake(Base):
    class Config:
        alias_generator = to_snake_case
        allow_population_by_field_name = True


class BaseCamel(Base):
    class Config:
        alias_generator = to_camel_case
        allow_population_by_field_name = True


class Message(Base):
    message: str
