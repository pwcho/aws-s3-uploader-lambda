from starlette.routing import Mount, Route

from app.config import config
from app.controllers import upload_file_controller

routes = [
    Mount(
        f"/{config.API_DEFAULT_PREFIX}",
        name="api_prefix",
        routes=[
            Mount(
                f"/{config.API_VERSION}",
                name="api_version",
                routes=[
                    Route("/s3/file", upload_file_controller, methods=["POST"]),
                ],
            )
        ],
    )
]
