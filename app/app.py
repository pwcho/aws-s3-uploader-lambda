import os
import traceback

from starlette.applications import Starlette
from starlette.exceptions import HTTPException
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR

from app.routes import routes


async def handle_http_exception(request: Request, exc: HTTPException):
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code, headers=exc.headers)


async def handle_normal_exception(request: Request, exc: Exception):
    content = {"detail": str(exc)}
    if os.getenv("DEBUG", False):
        content["debug"] = traceback.format_tb(exc.__traceback__)
    return JSONResponse(content, status_code=HTTP_500_INTERNAL_SERVER_ERROR, headers={"WWW-Authenticate": "Bearer"})


exception_handlers = {HTTPException: handle_http_exception, Exception: handle_normal_exception}

# stupid comment


def get_application():
    app = Starlette(
        debug=False,
        routes=routes,
        middleware=[Middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["GET", "POST", "PUT", "DELETE", "*"], allow_headers=["Authorization"])],
        exception_handlers=exception_handlers,
    )
    return app


app = get_application()
