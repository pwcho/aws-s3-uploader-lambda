import os

from pydantic import BaseSettings


class LambdaConfig(BaseSettings):
    SETUP_ENV: str = os.getenv("SETUP_ENV", "lambda")
    API_DEFAULT_PREFIX: str = os.getenv("API_DEFAULT_PREFIX", "api").strip(" /")
    API_VERSION: str = os.getenv("API_VERSION", "v1").strip(" /")
    AWS_S3_REGION: str = os.getenv("AWS_S3_REGION", "eu-central-1")
    AWS_S3_BUCKET: str = os.getenv("AWS_S3_BUCKET", None)


config = LambdaConfig()
