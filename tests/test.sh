curl -XPOST "http://localhost:9006/2015-03-31/functions/function/invocations" -d '
{
    "resource": "/api/v1/conv/encoded",
    "path": "/api/v1/conv/encoded",
    "httpMethod": "POST",
    "headers": {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-encoding": "gzip, deflate, br",
        "accept-language": "en-US,en;q=0.9",
        "cookie": "s_fid=7AAB6XMPLAFD9BBF-0643XMPL09956DE2; regStatus=pre-register",
        "Host": "70ixmpl4fl.execute-api.us-east-2.amazonaws.com",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "none",
        "upgrade-insecure-requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
        "X-Amzn-Trace-Id": "Root=1-5e66d96f-7491f09xmpl79d18acf3d050",
        "X-Forwarded-For": "52.255.255.12",
        "X-Forwarded-Port": "443",
        "X-Forwarded-Proto": "https",
        "Authorization": "Bearer eyJraWQiOiJlajJNalZBYnUrTVlPdjhLRllMZERxdlZwSjZzSGQ2ZjVubUtzK2pIOGdrPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI5YWQwODZkZS02MGU1LTRmNmMtOGI4Yi0xY2IwZjE5MDQ1ZGEiLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAuZXUtY2VudHJhbC0xLmFtYXpvbmF3cy5jb21cL2V1LWNlbnRyYWwtMV9vZzg0cUJaUmkiLCJjbGllbnRfaWQiOiJpaDVqM3RicDkwMWhqb2xzaXFxdG5zc2I5Iiwib3JpZ2luX2p0aSI6IjY4YTg3NjlmLThkNzEtNDc2YS1iMmNjLWJkOTY5M2NkZDY2MCIsImV2ZW50X2lkIjoiODdkNDFiOTUtOTMyYi00YzI5LWIzMjEtNWUyODgyNDFlOTczIiwidG9rZW5fdXNlIjoiYWNjZXNzIiwic2NvcGUiOiJhd3MuY29nbml0by5zaWduaW4udXNlci5hZG1pbiIsImF1dGhfdGltZSI6MTY1NTQ3NzgzMiwiZXhwIjoxNjU1NTY0MjMyLCJpYXQiOjE2NTU0Nzc4MzIsImp0aSI6ImVjYWJjZWE3LTEzYjgtNDM3NS05MWI2LWQ4YWEyYWQwNTJiMiIsInVzZXJuYW1lIjoicHJtYXJraWUifQ.O4h8IaDR3kxTIJ8hPQCsRmLd6SyshBfXlN6JNVX9Fo-fkv3grQcmHnv5zqtBdmXLmuo4LML-Y2zudSLIlLrMkxBeqdjZ0wOHMi6R8z5QqSRKJkHZx81ApvpCzHaG0ZIxDKr6ygF_ffhGa_ojFEG9P7-JcPTFEkMPKDlP44mmpTWNQ0G9fK9X0iYy45n5NDnEUKQIX5JYHnUvkRw6P4KwyM9Xl4NhgtGnVtDlI8Vxsqhan5dYtceUrKN_-6os9fvE4bm_3BX00POP3Ij52T_xiGB_yROORhCSKll0i8s0nCcq6RIh7-JG-2XG0pssB3HS1oN4zLlqWp8PHEU3jLdN8Q"
    },
    "multiValueHeaders": {
        "accept": [
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
        ],
        "accept-encoding": [
            "gzip, deflate, br"
        ],
        "accept-language": [
            "en-US,en;q=0.9"
        ],
        "cookie": [
            "s_fid=7AABXMPL1AFD9BBF-0643XMPL09956DE2; regStatus=pre-register;"
        ],
        "Host": [
            "70ixmpl4fl.execute-api.ca-central-1.amazonaws.com"
        ],
        "sec-fetch-dest": [
            "document"
        ],
        "sec-fetch-mode": [
            "navigate"
        ],
        "sec-fetch-site": [
            "none"
        ],
        "upgrade-insecure-requests": [
            "1"
        ],
        "User-Agent": [
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36"
        ],
        "X-Amzn-Trace-Id": [
            "Root=1-5e66d96f-7491f09xmpl79d18acf3d050"
        ],
        "X-Forwarded-For": [
            "52.255.255.12"
        ],
        "X-Forwarded-Port": [
            "443"
        ],
        "X-Forwarded-Proto": [
            "https"
        ]
    },
    "queryStringParameters": null,
    "multiValueQueryStringParameters": null,
    "pathParameters": null,
    "stageVariables": null,
    "requestContext": {
        "resourceId": "2gxmpl",
        "resourcePath": "/",
        "httpMethod": "GET",
        "extendedRequestId": "JJbxmplHYosFVYQ=",
        "requestTime": "10/Mar/2020:00:03:59 +0000",
        "path": "/Prod/",
        "accountId": "123456789012",
        "protocol": "HTTP/1.1",
        "stage": "Prod",
        "domainPrefix": "70ixmpl4fl",
        "requestTimeEpoch": 1583798639428,
        "requestId": "77375676-xmpl-4b79-853a-f982474efe18",
        "identity": {
            "cognitoIdentityPoolId": null,
            "accountId": null,
            "cognitoIdentityId": null,
            "caller": null,
            "sourceIp": "52.255.255.12",
            "principalOrgId": null,
            "accessKey": null,
            "cognitoAuthenticationType": null,
            "cognitoAuthenticationProvider": null,
            "userArn": null,
            "userAgent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36",
            "user": null
        },
        "domainName": "70ixmpl4fl.execute-api.us-east-2.amazonaws.com",
        "apiId": "70ixmpl4fl"
    },
    "body": "{\"encoded\": \"CjoyMDoxCjoyNTovUEw3NzEyNDA1NDk3MTExMTAwMDA1MDA5NzQ5Ngo6MjhDOjAwMDAyCjo2MEY6QzIwMDEwMlBMTjAwMDAwNTc5OTQ4MCw2OAo6NjE6MjAwMTAyMDEwMkNOMDAwMDA2NzgzNzYzLDYxTjg0M05PTlJFRgo6ODY6ODQzXjAwUFJaRUtTScSYR09XQU5JRSBTQUxEQSBMT0tBVF4zNDAwMApeMzAxMjQwNTQ5N14zODc3MTI0MDU0OTcxMTExMDAwMDUwMDk3NDk2Cl4yME9QUk9DRU5UT1dBTklFICAwLjEzMDAwMCAlCjo2MToyMDAxMDIwMTAyQ04wMDAwMDAyMTM4MDYsMzJOMDUxTk9OUkVGCjo4NjowNTFeMDBQUlpFTEVXICAgICAgICAgICAgICAgICAgICBeMzQwMDAKXjMwMTI0MDI1MDBeMzhQTDU5MTI0MDI1MDAxMTExMDAwMDM3Nzc3ODEyCl4yMDAxMDYyMS8wMDEvMTlIIDAxMDcyMS8wMDEvMV4yMTlIICAwMTA3MjIvMDAxLzE5SCAwMTA3MjMvMApeMjIwMS8xOUggIDAxMDcyNC8wMDEvMTlICl4zMlNUT0tST1RLQSBTUC4gWiBPLk8uICAgICAgIF4zMyAgICAgICAgUFJPSkVLVE9XQSBOUiAxCl42MjIwLTIwOSBMVUJMSU4KOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDE5Mzc4OCwwME4wNTFOT05SRUYKOjg2OjA1MV4wMFBSWkVMRVcgS1JBSk9XWSBNScSYRFpZQkFOS09eMzQwMDAKXjMwMTE0MDExMjReMzhQTDc5MTE0MDExMjQwMDAwNDUwMDg1MDAxMDAxCl4yMDk2NzIsOTY3Miw5NjczLDk2NzMsOTY3NCw5Nl4yMTc0LCAgICAgOTY3NSw5Njc1LDk2NzYsOTY3NgpeMjIsOTY3Nyw5Njc3LCAgICAgOTY3OCw5Njc4LDleMjM2NzksOTY3OSw5NjgwLDk2ODAsICAgICBLUkUKXjI0RDAwMDAwMDM4MjcKXjMyTElETCBTUC4gWiBPLk8uIFNQLiBLT01BTkRZXjMzVE9XQSAgICBQT1pOQcWDU0tBIDQ4Cl42MjYyLTA4MCBKQU5LT1dJQ0UKOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDE2MTcwMCwwME4wNTFOT05SRUYKOjg2OjA1MV4wMFBSWkVMRVcgS1JBSk9XWSBNScSYRFpZQkFOS09eMzQwMDAKXjMwMTYwMDExMjdeMzhQTDg1MTYwMDExMjcwMDAzMDEyMjA2MDUzMDAxCl4yMDYzMzogMTA3MDIuMTA3NTkvMDAxLzE5aApeMzJJTlRFUkZPT0QgUE9MU0tBICAgICAgICAgICBeMzMgICAgICAgIFNQw5PFgUtBIFogTy5PLgpeNjJVTC4gTUFEQUxJxYNTS0lFR08gMjAgTE9LLjFDXjYzICAgICAgICAwMi01MTMgV0FSU1pBV0EKOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDE0NzEzNCwyNE4wNTNOT05SRUYKOjg2OjA1M14wMFBSWkVMRVcgUE9EWklFTE9OWSBCRVRBL0lOVF4zNDAwMApeMzAxMjQwMjM5NV4zOFBMOTYxMjQwMjM5NTExMTEwMDEwNjU2NDA1NjcKXjIwL1ZBVC83MDA2LDM5L0lEQy83MTYwMDAyMTY0XjIxL0lOVi8wMTAwNzkvMDAxLzE5SC9UWFQvMDEwCl4yMjA3OS8wMDEvMTlICl4zMk1BTC1QTEFTVC5QSFUgSy5SWU1BUkNaWUsuQl4zM1VSREEtQlJBTkQKXjYyU0tSWVRLQSBQT0NaVE9XQSA5OCAgICAgICAgXjYzICAgICAgICAyMy0yMDAgS1JBxZpOSUsKOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDA4MjgzNCw1ME4wNTFOT05SRUYKOjg2OjA1MV4wMFBSWkVMRVcgS1JBSk9XWSBNScSYRFpZQkFOS09eMzQwMDAKXjMwMTAzMDEwMTZeMzhQTDE3MTAzMDEwMTYwMDAwMDAwMDQ3MTEzMDAwCl4yMGYudmF0IDEwMjk0LzE5Cl4zMkNPTlRSQUNUVVMgU1AgWiBPIE8gICAgICAgIF4zMyAgICAgICAgVUwuIEJVS09XScWDU0tBIDI0QQpeNjJMT0suIDU4ICAgICAgICAgMDItNzAzIFdBUlNeNjNaQVdBCjo2MToyMDAxMDIwMTAyQ04wMDAwMDAwNzk0NjQsMDBOMDUzTk9OUkVGCjo4NjowNTNeMDBQUlpFTC5QT0RaSUVMT05ZIFBSWllDSC5NScSYXjM0MDAwCl4zMDEwMzAxNTA4XjM4UEw5MTEwMzAxNTA4MDAwMDAwMDgxNjAzMzA0NwpeMjAvVkFULzM3ODQsMDAvSURDLzcxNjAwMDIxNjReMjEvSU5WLzAxMDUzMy8wMDEvMTlIL1RYVC81MTAKXjIyMDA1NTU3Ni8KXjMyQ0FSR0lMTCBQT0xBTkQgU1AuIFogTy5PLiAgXjMzICAgICAgICBVTC5XT0xPU0tBIDIyCl42MjAyLTY3NSBXQVJTWkFXQQo6NjE6MjAwMTAyMDEwMkNOMDAwMDAwMDc4Njg1LDkwTjA1MU5PTlJFRgo6ODY6MDUxXjAwUFJaRUxFVyBLUkFKT1dZIE1JxJhEWllCQU5LT14zNDAwMApeMzAxMDIwNDExNV4zOFBMODExMDIwNDExNTAwMDA5MjAyMDA2NDcyNTUKXjIwUFJaRURQxYFBVEEgRE8gWkFNw5NXSUVOSUEgMDBeMjEwMDI0LzIwLzAwMSBNSU5VUyBLT1JFS1RZIDAKXjIyMDIxMDUvMDAyMTc4IE9SQVogTkFEUMWBLiBaIF4yMzIxLjExLTgzLDM2IFrFgQpeMzJQSFVQIEdOSUVaTk8gU1AuIFogTy5PLiBIVVJeMzNUT1dOSUEgU1DDk8WBS0EgS09NQU5EWVRPV0EKXjYyVUwuIE9SQ0hPTFNLQSA0MSAgICAgICAgICAgXjYzICAgICAgICA2Mi0yMDAgR05JRVpOTwo6NjE6MjAwMTAyMDEwMkNOMDAwMDAwMDY5NjI3LDY3TjA1MU5PTlJFRgo6ODY6MDUxXjAwUFJaRUxFVyBLUkFKT1dZIE1JxJhEWllCQU5LT14zNDAwMApeMzAxMDUwMDA4Nl4zOFBMMzExMDUwMDA4NjEwMDAwMDIzMjk5NDQ4ODQKXjIwMDIxMTI2NjIxMzUgTWFrcm8gUEwKXjMyTUVUUk8gSU5URVJOQVRJT05BTCBBRyAgICAgXjMzICAgICAgICBORVVIT0ZTVFJBU1NFIDQsIDYzCl42MjQxIEJBQVIKOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDA2NTQyNCwzNU4wNTFOT05SRUYKOjg2OjA1MV4wMFBSWkVMRVcgS1JBSk9XWSBNScSYRFpZQkFOS09eMzQwMDAKXjMwMTAzMDE1MDheMzhQTDQyMTAzMDE1MDgwMDAwMDAwNTYwNDkxMDE4Cl4yMFd5cMWCYXRhIHogdHl0dcWCdSAwMTA5NzQvMDAxXjIxLzE5SCBuYSBrd290xJkgNjU0MjQsMzUgUExOIApeMjJkbGEgU1DDk8WBRFpJRUxOSUFNTEVDWkFSU0tBIF4yM1JZS0kgdyBpbWllbml1IEVVUk9DQVNIIFMuQQpeMjQuLiBXaWVyenl0ZWxub8WbxIcgemfFgm9zem9uYSBeMjUwMi0wCl4zMlBST0dSQU1fU1VCUk9HQUNKQS9FVVJPQ0FTSF4zMyBTLkEgICAgVUwuIFdJU05JT1dBIDExCl42MjYyLTA1MiBLT01PUk5JS0kKOjYxOjIwMDEwMjAxMDJDTjAwMDAwMDA0NzMxNiwzNk4wNTFOT05SRUYKOjg2OjA1MV4wMFBSWkVMRVcgICAgICAgICAgICAgICAgICAgIF4zNDAwMApeMzAxMjQwMjcwMl4zOFBMMzgxMjQwMjcwMjExMTEwMDEwMzUyMjQyMzIKXjIwRiAwMTA5MDkvMDAxLzE5SApeMzJNQVJDSU4gRw==\"}",
    "isBase64Encoded": false
}'